**Author:Nipu Chakraborty**
<br>
**Gmail:nipuchakraborty86@gmail.com**
<br>
**facebook:https://www.facebook.com/pro.nipuchakraborty**
<br>


**Who is the founder of Reactjs?**

**Jordan Walke** is the Founder of reactjs.

![Jordan Walke](https://media.bizj.us/view/img/10393698/tom-occhino*750xx2535-1426-0-237.jpg)

**Developed By**


![Jordan Walke](https://duckduckgo.com/i/8fef8f06.png)







**What is React js?**


1. Reactjs is a popular javascript front-end libray
2. It's an open source Javascript and it's developed by facebook.com 2011
3. Follows component based approach
4. it allows to create reuseable Ui componenets
5. used to develop complex, interactive web as well as mobile UI
6. open source in 2015 and has strong foundation and large community



**What are the feacture of react?**

1. Uses virtual DOM
2. Does Server-side rendering
3. Follows Uni-derctional data flow i.e one way data binding




**List Some of the major advantage of reactjs?**

1. Increases application performance
2. Can be used on client as well as Server Side
3. Readability is improved
4. Easy to integrate
5. Easy to write UI test cases.


**What are the limitaion of the reactjs?**

1. Not a full scale framework i.e it's just the view
2. Library is quite large
3. Difficult to understand for novice Programmers
4. Uses inline templating and JSX


**What is JSX?**
1. JSX Stand for javascript XML
3. Makes HTML easy to Understand
4. It's Robust
5. Boosts up the js performance
6. JSX expression must have only once outermost element



**What do you understand by virtual DOM ? Explane it's working.**

**Lightwedth Javascript Object it's a copy of real DOM.
it's Work in simple steps:**

1. Whenever any underlying data the previous DOM represention and the new one is calculated
2. Then the deffrence between the previous DOM representaion and the new one is calculated.
3. Once the calculation are done , The reak DOM will be updated with only the things that have acually changed


**Why Browsers can't readJSX?**

1. JSX is not a reagular javasript.
2. Brosers Can read Jvascript Objects Only
3. JSX file is converted to js Object By JSX TRanceformaer like babel , Before reaching Browser.




**What is the defference between React and anglar?**


1. React is a SSR(Server Side Rendering) library and angular is an CSR(Client side rendering) Frame Work
2. React has virtual Dom and Angular has Real DOM
3. React work With one way data binding but angular two way data binding
4. React Duging Compaile time and angular Debuging Runtime
5. React is developed by facebook and angular is developed by google




**Explaine the pupose of render() in react?**

1. Every component must have a render()
2. It's returns single React element which is representation of native DOM componenet.
4. HTML elements inside render() must be enclsed inside an enclosing tag like `<div>`, `<group>` `<from>` etc.
5. should be pure function.



**What is props?**

1. props is short from for propertice
2. pros Read only
3. are pure i,e immutable
4. always passed down from parent to child component
5. used to render dynamic data



**What is sate?**

1. Heart of react components
2. Must be kept as imple as posible
3. deteemines kept as simple as possible 
4. determine components rendering and behaviour
5. create dynamic and interactive components
6. it is accessed via this.state()
7. can update the state using this,setState()


**What is defference between state and props?**

1. state and props Receves initial value from parent Components
 
2. state parent Components Cannot Change value but props can

3. state and props can set default values inside Component
 
4. state can Changes inside Component but props cannot change that 
 
5. state and props set initial value for child Componenets
 
6. and State can change inside child componenets but props can change that



**How can you update state of components ??**



1. Using this.setState() function you can change the states of the components 


**What is arrow function??**

1. Arrow function new es6 feature this is also called fat arrow function(`()=>`) 
2. allows to bind the contex of components properly sice auto-binding is ot avilable by default in es6.
3. Makes easier to Work with higher order functions.



**Explaine The lifecycle methods of react componenets in details**

1. componentWillMount(); :- this function excuted just before rendering both on client and server side 
2. ComponentDidMount() :- is executed after first render only on the client side
3. componentWillReciveProps():- is invoked as soon as the props are receved from parent class before another render is called 
4. shouldComponentUpdate() :- it's return true or false value based on certain condation. if you want your components to update return true else return falase .By default 
5. componentWillUpdate();:-> is called just before rendering takes place.
6. componentsDidUpdate():-> it's is called just after rendering takes place.
7. componentwillUnmount():-> is called after the component is unmounted from the dom.it is used to clear up the memory spaces.




**What is an event in react?**


events the triggered reactions to specific action like `mouseHover, onClick,onSubmit,onkeyPress` etc


**What is refs in reactjs?**

1. Refs Stands for References
2. Used to returns to a particular element or components returned by render()
3. useful When we need DOM measurement or to add methods to the components



**Give some example When should use refs?**

1. Managing focus selection or media playback
2. Triggering imperative animations
3. Intergrating with third-party DOM libraries.



**How froms created in react ?**

1. HTML from elements maintain their own state in regular DOMS and update themselves based on user inputs.
2. In React state is Contained in the state property of the component and is only updated via setState().
3. Javascript function is used for handling the from submission



**What are pure component?**


1. Pure components are the simlest ,fastest Components which we can write .
2. Can replace any Components that only has render();
3. Enhance the simplicity and performance of the application.



**What are the major prolems with MVC framework?**

1. DOM manipulation was very expensive
2. slow and inefficient
3. Memory wastage.
4. Beacause of circular dependencies, Comlicated modal was created around models and views.


**What is flux?**

1. Architechural pattern that enfroces uni-directional data flow.
2. Controls derived data and enables comunication between multiple components
3. Contains a central Store which has authority for all data 
4.any update in data must ocur here only
5. Provides stability to the application.
6. reduces run-times errors



**What is redux?**

1. Redux on of the hottest libraries for front end developement
2. Redux is a predictable state container for Javascript apps
3. Mostly used for application state Management.
4. Application developed with Redux are easy to test.
5. Helps to Write applications that behave consistently and run in deffresnt environments.
